package me.jvt.hacking.model;

public record Api(String name, String url) {}
