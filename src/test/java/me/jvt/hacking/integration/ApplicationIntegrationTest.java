package me.jvt.hacking.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class ApplicationIntegrationTest {
  @Autowired private MockMvc mvc;

  @Test
  void contextLoads(ApplicationContext context) {
    assertThat(context).isNotNull();
  }

  @Test
  void unauthenticatedRequestsToNonexistentEndpointReturns401() throws Exception {
    mvc.perform(get("/info")).andExpect(status().isUnauthorized());
  }

  @Test
  void authenticatedRequestsToNonexistentEndpointReturns404() throws Exception {
    mvc.perform(get("/info").with(httpBasic("user", "password"))).andExpect(status().isNotFound());
  }
}
