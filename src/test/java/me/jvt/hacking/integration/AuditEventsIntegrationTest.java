package me.jvt.hacking.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class AuditEventsIntegrationTest {
  @Autowired private MockMvc mvc;
  @Autowired private AuditEventRepository repository;

  @Test
  void auditRepositoryIsFound() {
    assertThat(repository).isNotNull();
  }

  @Test
  void unauthenticatedRequestsToNonexistentEndpointStoresEventInRepository() throws Exception {
    mvc.perform(get("/info"));

    assertThat(repository.find("anonymousUser", null, "AUTHORIZATION_FAILURE"))
        .hasSizeGreaterThanOrEqualTo(1);
  }

  @Test
  void authenticatedRequestsToNonexistentEndpointStoresEventInRepository() throws Exception {
    mvc.perform(get("/info").with(httpBasic("user", "password")));

    assertThat(repository.find("user", null, "AUTHENTICATION_SUCCESS"))
        .hasSizeGreaterThanOrEqualTo(1);
  }
}
